﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Microsoft.Extensions.Configuration;
using UrTCommunityBot.Models;

namespace UrTCommunityBot
{
    public partial class Commands : ModuleBase<SocketCommandContext>
    {
        readonly CommandService service;
        private UrbanTerrorScraper _scrapper;

        private IConfiguration _config;

        private UrTCommunityBotContext _dbContext;

        public Commands(CommandService service, 
                        UrbanTerrorScraper scraper, 
                        IConfiguration config,
                        UrTCommunityBotContext dbContext)
        {
            this.service = service;
            _scrapper = scraper;
            _config = config;
            _dbContext = dbContext;
        }

        [Command("ping")]
        [Summary("Test command")]
        public async Task Ping()
        {
            await ReplyAsync("Pong");
        }

        [Command("lastseen")]
        [Summary("Last seen")]
        public async Task LastSeen(string auth)
        {
            Player player = await _scrapper.CreatePlayerInfoAsync(auth);
            
            string msg = "";
            if (!String.IsNullOrEmpty(player.LastSeen))
                msg = $"{player.Name} was last seen {player.LastSeen}";
            else 
                msg = $"{player.Auth} was not found";


            var registeredPlayer = _dbContext.Players.SingleOrDefault(p => p.Auth == auth);
            if (registeredPlayer != null)
            {
                registeredPlayer.LastSeen = player.LastSeen;
                registeredPlayer.LastDateTime = player.LastDateTime;

                _dbContext.Players.Update(registeredPlayer);
                await _dbContext.SaveChangesAsync();
            }

            await ReplyAsync(msg);

        }
       
        [Command("scraper")]
        [Summary("List scraper info")]
        public async Task ScraperInfo()
        {
            string msg = $"Players: {UrbanTerrorScraper.Players.Count}\nServers: {UrbanTerrorScraper.Servers.Count}";
            await ReplyAsync(msg);
        }

        [Command("online")]
        [Summary("Find online players")]
        public async Task Find(string query)
        {
            var list = _scrapper.FindPlayers(query);
            string msg = $"**Players: ** ({list.Count} results)\n";
            if (list.Count > 0 && list.Count < 13)
            {
                foreach (var x in list)
                {
                    msg += $"> :person_frowning: **{x.Name,-25} ** :desktop: `{x.Server.Name,-35}` :link: <urt://{x.Server.Address}>\n";
                }
            }
            else if (list.Count >= 13)
            {
                msg += ":red_square: **Too many results!!**";
            }
            else
            {
                msg += ":red_square: **No players found**";
            }

            var lines = msg.Split('\n');
            string multilineMsg = "";

            for (int i = 0; i < lines.Length; i++)
            {
                multilineMsg += lines[i] + "\n";

                if (i + 1 == lines.Length || (i % 4 == 0 && i != 0))
                {
                    await ReplyAsync(multilineMsg);
                    await Task.Delay(250);
                    multilineMsg = "";
                }
            }
        }


        [Command("help")]
        [Summary("Get all available commands")]
        public async Task HelpAsync()
        {
            var prefix = this._config.GetSection("Discord:CommandPrefix");
                        
            var builder = new EmbedBuilder()
            {
                Color = new Color(206, 206, 30),
                Description = "**Available commands:**"
            };

            foreach (var module in service.Modules)
            {
                string description = null;
                foreach (var cmd in module.Commands)
                {
                    var result = await cmd.CheckPreconditionsAsync(Context);
                    if (result.IsSuccess)
                        description += $"{prefix.Value}{cmd.Aliases.First()}\n";
                }

                if (!string.IsNullOrWhiteSpace(description))
                {
                    builder.AddField(x =>
                    {
                        x.Name = module.Name;
                        x.Value = description;
                        x.IsInline = false;
                    });
                }
            }

            await ReplyAsync("", false, builder.Build());
        }
        /*
        [Command("help")]
        [Summary("Get Help for specific command")]
        public async Task HelpAsync(string command)
        {
            var result = service.Search(Context, command);

            if (!result.IsSuccess)
            {
                await ReplyAsync($"Sorry, I couldn't find a command like **{command}**.");
                return;
            }

            string prefix = "!";
            var builder = new EmbedBuilder()
            {
                Color = new Color(114, 137, 218),
                Description = $"Here are some commands like **{command}**"
            };

            foreach (var match in result.Commands)
            {
                var cmd = match.Command;

                builder.AddField(x =>
                {
                    x.Name = string.Join(", ", cmd.Aliases);
                    x.Value = $"Parameters: {string.Join(", ", cmd.Parameters.Select(p => p.Name))}\n" +
                              $"Summary: {cmd.Summary}";
                    x.IsInline = false;
                });
            }

            await ReplyAsync("", false, builder.Build());
        }
        */
    }
}
