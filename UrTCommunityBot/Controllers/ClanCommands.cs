using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using UrTCommunityBot.Models;
using UrTCommunityBot.Utils;

namespace UrTCommunityBot
{
    public partial class Commands : ModuleBase<SocketCommandContext>
    {

        [Command("members")]
        [Summary("List all members")]
        public async Task Members()
        {
            var clan = await _dbContext.Clans
                .Include(l => l.Leaders)
                .Include(m => m.Members)
                .FirstOrDefaultAsync();

            if (clan == null)
            {
                await ReplyAsync($"Clan is not configured yet");
                return;
            }

            // No leaders yet
            String msg = "";
            if (clan.Leaders.Count > 0)
            {
                msg = $"**{clan.Tag} Leading team:**\n";
                foreach (var p in clan.Leaders)
                {
                    
                    msg += $":flag_{p.Flag}: {Utils.Discord.Escape(p.Name)} ({p.Auth}) - last seen: {p.LastSeen} \n";
                }
                msg += "\n";
            }

            // No players yet
            if (clan.Members.Count == 0)
            {
                msg += $"This clan has no members yet";
            }
            else
            {
                var activePlayers = clan.Members.Where(p => p.LastDateTime > DateTime.Now.AddMonths(-2))
                    .OrderByDescending(p=>p.LastDateTime);
                if (activePlayers != null && activePlayers.Count() > 0)
                {
                    msg += $"**{clan.Tag} Active players:**\n";
                    foreach (var p in activePlayers)
                    {
                        msg += $":flag_{p.Flag}: {Utils.Discord.Escape(p.Name)} ({p.Auth}) - last seen: {Utils.Discord.Escape(p.LastSeen)} \n";
                    }
                }
                
                var inactivePlayers = clan.Members.Where(p => p.LastDateTime <= DateTime.Now.AddMonths(-2))
                    .OrderByDescending(p=>p.LastDateTime);
                if (inactivePlayers != null && inactivePlayers.Count() > 0)
                {
                    msg += $"\n**{clan.Tag} Inactive players:**\n";
                    foreach (var p in inactivePlayers)
                    {
                        msg += $":flag_{p.Flag}: {Utils.Discord.Escape(p.Name)} ({p.Auth}) - last seen: {Utils.Discord.Escape(p.LastSeen)} \n";
                    }
                }
            }
            await ReplyAsync(msg);
        }

        [Command("server")]
        [Summary("List Server Players info")]
        public async Task ClanServers()
        {

            var clan = await _dbContext.Clans
                .Include(s => s.Servers)
                .FirstOrDefaultAsync();

            if (clan == null)
            {
                await ReplyAsync("No clans configured yet");
                return;                
            } 

            if (clan.Servers.Count == 0)
            {
                await ReplyAsync("No servers configured yet");
                return;                
            }

            string msg = "";

            foreach (var server in clan.Servers)
            {
                var sp = await _scrapper.GetServerPlayersAsync(server.Address);
                var flag = !String.IsNullOrEmpty(sp.Server.Flag) ? $":flag_{sp.Server.Flag}:" : "";
                msg += $"**Server: ** {flag} {sp.Server.Name} (:link: <urt://{sp.Server.Address}>)\n";
                msg += $"**Map: ** {sp.Server.CurrentMap} **Game type:** {sp.Server.GameType} \n";
                msg += $"**Players: ** ";
                if (sp.Players.Count > 0)
                {
                    foreach (var player in sp.Players)
                    {
                        msg += $"{player.Name} ";
                    }
                }
                else
                    msg += "Empty";

                msg += "\n";
            }
            
            //string msg = $"Server: {UrbanTerrorScraper.Players.Count}\nServers: {UrbanTerrorScraper.Servers.Count}";
            await ReplyAsync(msg);
        }
    }
}
