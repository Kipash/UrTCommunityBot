using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using UrTCommunityBot.Models;

namespace UrTCommunityBot
{
    public partial class Commands : ModuleBase<SocketCommandContext>
    {

        [Command("iamgod")]
        [Summary("Register Admin commands")]
        public async Task Iamgod()
        {
            var god = _dbContext.DiscordAdmins.SingleOrDefault(a => a.IsGod);
            
            // No admin yet
            if (god == null)
            {
                Administrator admin = new Administrator 
                { 
                    DiscordId = this.Context.User.Id,
                    DiscordName = this.Context.User.Username,
                    IsGod = true
                };
                
                _dbContext.DiscordAdmins.Add(admin);
                _dbContext.SaveChanges();

                await ReplyAsync("Yes, you're now GOD!!!");

            } else if (god.DiscordId == this.Context.User.Id) 
            {
                await ReplyAsync("You already are my GOD!!!");
            } else 
            {
                await ReplyAsync("Sorry, I already have my god!!!");
            }
        }

        [Command("addclan", false)]
        [Summary("Adds a new clan")]
        public async Task AddClan([Remainder]String command)
        {
            var god = _dbContext.DiscordAdmins.SingleOrDefault(a => a.DiscordId == this.Context.User.Id);

            // No admin yet
            if (god == null)
            {
                await ReplyAsync("Sorry, you don't have permissions to do that");
                return;
            }
                          
            string[] words = command.Split(" ", StringSplitOptions.RemoveEmptyEntries);
            if (String.IsNullOrEmpty(command) || words.Length < 2)
            {
                await ReplyAsync("Command usage: <tag> <clan name>");
                return;
            }

            var clan = _dbContext.Clans;
            if (clan.Count() > 0)
            {   
                await ReplyAsync("Clan configuration already exists");
                return;
            }

            Clan c = new Clan { 
                ClanId = Guid.NewGuid().ToString(),
                Tag = words[0],
                Name = string.Join(" ", words)
            };

            _dbContext.Clans.Add(c);          
            await _dbContext.SaveChangesAsync();

            await ReplyAsync("Clan successfully added");
        }

        [Command("addleader")]
        [Summary("Adds a new leader to the clan")]
        public async Task AddLeader(SocketUser user = null, String playerAuth = "")
        {
            var god = _dbContext.DiscordAdmins.SingleOrDefault(a => a.DiscordId == this.Context.User.Id);

            // No admin yet
            if (god == null)
            {
                await ReplyAsync("Sorry, you don't have permissions to do that");
                return;
            }

            if (user == null || String.IsNullOrEmpty(playerAuth))
            {
                await ReplyAsync("Command usage: @DiscordName auth");
                return;
            }
            
            var clan = await _dbContext.Clans
                .Include(l => l.Leaders)
                .FirstOrDefaultAsync();

            if (clan == null)
            {
                await ReplyAsync($"Clan is not configured yet");
                return;
            }

            var player = clan.Leaders.SingleOrDefault(p => p.DiscordId == user.Id);
            if (player != null)
            {
                await ReplyAsync($"{user.Mention} is already a leader");
                return;
            }
            else 
            {
                Player urtPlayer = await _scrapper.CreatePlayerInfoAsync(playerAuth);
                
                if (urtPlayer == null || (urtPlayer != null && String.IsNullOrEmpty(urtPlayer.LastSeen)))
                {
                    await ReplyAsync($"{urtPlayer.Auth} not found on Urban Terror site");
                    return;
                }

                urtPlayer.PlayerId = Guid.NewGuid().ToString();
                urtPlayer.DiscordId = user.Id;
            
                clan.Leaders.Add(urtPlayer);

                _dbContext.Clans.Update(clan);
                await _dbContext.SaveChangesAsync();

                await ReplyAsync($"Successfully added new leader {urtPlayer.Name}");
            }
        }

        [Command("addmember")]
        [Summary("Adds a new member to the clan")]
        public async Task AddMember(SocketUser user = null, String playerAuth = "")
        {
            var god = _dbContext.DiscordAdmins.SingleOrDefault(a => a.DiscordId == this.Context.User.Id);

            // No admin yet
            if (god == null)
            {
                await ReplyAsync("Sorry, you don't have permissions to do that");
                return;
            }

            if (user == null || String.IsNullOrEmpty(playerAuth))
            {
                await ReplyAsync("Command usage: @DiscordName auth");
                return;
            }

            var clan = await _dbContext.Clans
                .Include(l => l.Members)
                .FirstOrDefaultAsync();
            if (clan == null)
            {
                await ReplyAsync($"Clan is not configured yet");
                return;
            }
            
            if (clan.Members != null)
            {
                var player = clan.Members.SingleOrDefault(p => p.DiscordId == user.Id || p.Auth == playerAuth);
                if (player != null)
                {
                    await ReplyAsync($"{user.Mention} is already a member");
                    return;
                }
            }
            
            Player urtPlayer = await _scrapper.CreatePlayerInfoAsync(playerAuth);
            
            if (urtPlayer == null || (urtPlayer != null && String.IsNullOrEmpty(urtPlayer.LastSeen)))
            {
                await ReplyAsync($"{urtPlayer.Auth} not found on Urban Terror site");
                return;
            }

            urtPlayer.PlayerId = Guid.NewGuid().ToString();
            urtPlayer.DiscordId = user.Id;

            clan.Members.Add(urtPlayer);
            _dbContext.Clans.Update(clan);
            await _dbContext.SaveChangesAsync();

            await ReplyAsync("Successfully added");
        }

        [Command("rmmember")]
        [Summary("Removes a member from the clan")]
        public async Task RemoveMember(string playerAuth = "")
        {
            var god = _dbContext.DiscordAdmins.SingleOrDefault(a => a.DiscordId == this.Context.User.Id);

            // No admin yet
            if (god == null)
            {
                await ReplyAsync("Sorry, you don't have permissions to do that");
                return;
            }

            if (String.IsNullOrEmpty(playerAuth))
            {
                await ReplyAsync("Command usage: auth");
                return;
            }

            var clan = await _dbContext.Clans
                .Include(l => l.Leaders)
                .FirstOrDefaultAsync();
            if (clan == null)
            {
                await ReplyAsync($"Clan is not configured yet");
                return;
            }

            if (clan.Members == null)
            {
                await ReplyAsync($"Clan has no members");
                return;
            }

            var player = clan.Members.SingleOrDefault(p => p.Auth == playerAuth.ToLower());
            if (player == null)
            {
                await ReplyAsync("That player is not found");
                return;
            }

            clan.Members.Remove(player);
        
            _dbContext.Clans.Update(clan);
            await _dbContext.SaveChangesAsync();

            await ReplyAsync("Successfully removed");
            
        }

        [Command("addserver")]
        [Summary("Adds a new server to the clan")]
//        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task AddServer(String addr = "")
        {
            var god = _dbContext.DiscordAdmins.SingleOrDefault(a => a.DiscordId == this.Context.User.Id);

            // No admin yet
            if (god == null)
            {
                await ReplyAsync("Sorry, you don't have permissions to do that");
                return;
            }

            if (String.IsNullOrEmpty(addr))
            {
                await ReplyAsync("Command usage: server");
                return;
            }

            var clan = await _dbContext.Clans
                .Include(s => s.Servers)
                .FirstOrDefaultAsync();
            if (clan == null)
            {
                await ReplyAsync($"Clan is not configured yet");
                return;
            }

            if (clan.Servers != null)
            {
                var server = clan.Servers.SingleOrDefault(s => s.Address == addr);
                if (server != null)
                {   
                    await ReplyAsync("That server already exists");
                    return;
                }
            }

            Server urtServer = await _scrapper.CreateServerInfoAsync(addr);
            
            if (urtServer == null)
            {
                await ReplyAsync($"Server with the address {addr} was not found on Urban Terror site");
                return;
            }

            urtServer.ServerId = Guid.NewGuid().ToString();

            clan.Servers.Add(urtServer);
            _dbContext.Clans.Update(clan);
            await _dbContext.SaveChangesAsync();

            await ReplyAsync("Server successfully added");
        }

        [Command("remserver")]
        [Summary("Removes a server from the clan")]
        public async Task RemoveServer(String addr = "")

        {
            var god = _dbContext.DiscordAdmins.SingleOrDefault(a => a.DiscordId == this.Context.User.Id);

            // No admin yet
            if (god == null)
            {
                await ReplyAsync("Sorry, you don't have permissions to do that");
                return;
            }

            if (String.IsNullOrEmpty(addr))
            {
                await ReplyAsync("Command usage: server");
                return;
            }

            var clan = await _dbContext.Clans
                .Include(s => s.Servers)
                .FirstOrDefaultAsync();

            if (clan == null)
            {
                await ReplyAsync($"Clan is not configured yet");
                return;
            }

            if (clan.Servers == null)
            {
                await ReplyAsync($"This clan has no servers yet");
                return;
            }

            var server = clan.Servers.SingleOrDefault(s => s.Address == addr);
            if (server == null)
            {   
                await ReplyAsync("That server does not exist");
                return;
            }

            clan.Servers.Remove(server);
            _dbContext.Clans.Update(clan);
            await _dbContext.SaveChangesAsync();

            await ReplyAsync("Server successfully removed");
        }
    }
}
