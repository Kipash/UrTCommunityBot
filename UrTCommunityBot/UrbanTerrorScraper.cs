﻿using HtmlAgilityPack;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using UrTCommunityBot.Models;
using System.Threading.Tasks;
using UrTCommunityBot.Services;

public class UrbanTerrorScraper
{
    public static List<Server> Servers = new List<Server>();
    public static List<Player> Players = new List<Player>();

    static List<Server> tempServers = new List<Server>();
    static List<Player> tempPlayers = new List<Player>();

    public HttpClientWraper _httpWrapper;

    public UrbanTerrorScraper(HttpClientWraper wrapper)
    {
        _httpWrapper = wrapper;
    }

    public void LoadAllServers()
    {
        var masterList = _httpWrapper.UrtHttpClient.GetStringAsync("/servers/master/?empty=0");

        Regex ipPattern = new Regex(@"\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}:\d{1,5}");
        var matches = ipPattern.Matches(masterList.Result.ToString());

        if (matches.Count == 0)
        {
            Console.WriteLine("ERROR: no data from master list");
        }
        else
        {
            Console.WriteLine($"masterList list: {matches.Count}");
        }

        foreach (Match x in matches)
        {
            tempServers.Add(new Server(x.Value));
        }

        if (Servers.Count == 0)
            Servers = tempServers;

    }

    public async Task<Player> CreatePlayerInfoAsync(string user_auth)
    {
        try
        {
            
            Player p = new Player() { Auth = user_auth };

            var httpResponseMessage = await _httpWrapper.UrtAuthHttpClient.GetStringAsync($"/members/profile/{user_auth}/");
            var doc = new HtmlDocument();
            doc.LoadHtml(httpResponseMessage);

            var boardPath = @"//center/div/div[@id='board']/div[@id='main']/div[@id='mainBox']";
            var boardNode = doc.DocumentNode.SelectSingleNode(boardPath);
            if (boardNode == null)
            {
                Console.WriteLine("BoardNode was missing on: " + p.Name);
                return p;
            }
        
            if (boardNode.SelectSingleNode("div[@class='errorBox']") != null)
                return p;

            // Player name
            p.Name = p.Auth;

            string tag = "";
            if (boardNode.SelectSingleNode("//span[@class='userTag start']") != null)
                tag = boardNode.SelectSingleNode("//span[@class='userTag start']").InnerText;
                
            var username = boardNode.SelectSingleNode("//span[@class='userName']").InnerText;
            
            p.Name = tag + username;

            // Player info
            var playerInfoNode = boardNode.SelectSingleNode("//table[@class='infos']");

            // Gender
            var gender = playerInfoNode.SelectSingleNode("tr[td/b[text()='Sex:']]/td/text()");
            p.Gender = gender != null ? gender.InnerText : "";

            // Country flag
            var flag = playerInfoNode.SelectSingleNode("tr[td/b[text()='Country:']]/td/span");
            p.Flag = flag != null ? flag.GetAttributeValue("alt","") : "";

            // Last game
            var lastGameNode = playerInfoNode.SelectSingleNode("tr[td/b[text()='Last game:']]");
            
            string lastGame = "";
            if (lastGameNode.SelectSingleNode("td/span/text()") != null && lastGameNode.SelectSingleNode("td/text()") != null)
                lastGame = lastGameNode.SelectSingleNode("td/span/text()").InnerText + lastGameNode.SelectSingleNode("td/text()").InnerText;
            else if (lastGameNode.SelectSingleNode("td/font/text()") != null)
                lastGame = lastGameNode.SelectSingleNode("td/font/text()").InnerText;
            
            DateTime lstGame = DateTime.MinValue;
            if (lastGameNode.SelectSingleNode("td/span") != null) 
            {
                var lastDate = lastGameNode.SelectSingleNode("td/span").GetAttributeValue("title","");
                lastDate = Regex.Replace(lastDate, "\\+[0-9]{4} ", "");
                DateTime.TryParse(lastDate, out lstGame);
            }

            p.LastDateTime = lstGame;


            lastGame = lastGame.Replace("&nbsp;"," ");

            // Last server
            var lastServerNode = playerInfoNode.SelectSingleNode("//tr[td/b[text()='Last server:']]/td/a");

            var lastServer = lastServerNode.GetAttributeValue("href","");
            lastServer = lastServer.Replace("servers","");
            lastServer = lastServer.Replace("/","");

            var lastServerName = lastServerNode.InnerText;
            
            p.LastSeen = $"{lastGame} @ {lastServerName} <urt://{lastServer}>"; 

            Console.WriteLine($"Last seen: {p.LastSeen}");

            return p;

        }
        catch (System.Exception ex)
        {
            Console.WriteLine($"Exception {ex}");
        }   

        return null; 
    }

    public async void CreateServerInfo(Server s, bool duplicate = false)
    {
        var address = s.Address;

        string server = await _httpWrapper.UrtHttpClient.GetStringAsync($"/servers/{address}/");
        var doc = new HtmlDocument();
        doc.LoadHtml(server);

        //foreach (var x in doc.ParseErrors)
        //{
        //    Console.WriteLine($"err: {x.SourceText}");
        //}

        var boardPath = @"//center/div/div[@id='board']/div[@id='main']/div[@id='mainBox']";
        var boardNode = doc.DocumentNode.SelectSingleNode(boardPath);
        if (boardNode == null)
        {
            Console.WriteLine("BoardNode was missing on: " + s.Address);
            return;
        }

        var nameNode = boardNode.SelectSingleNode("h2");
        if (nameNode == null)
        {
            Console.WriteLine("NameNode was missing on: " + s.Address);
            return;
        }

        s.Name = nameNode.InnerText;
        //Console.WriteLine($"Server name: {s.Name}");
        //Console.WriteLine($"IP: {address}");

        var playerBoard = boardNode.SelectSingleNode("div[@class='darkerBox']/table[@class='list']");

        if (playerBoard != null)
        {
            //Console.WriteLine($"Players: ");
            var players = playerBoard.Elements("tr");
            for (int i = 1; i < players.Count(); i++)
            {
                var player = players.ElementAt(i);
                var plname = player.ChildNodes[3];

                var p = new Player() { Name = plname.InnerText, Server = s };
                tempPlayers.Add(p);
                if (duplicate)
                    Players.Add(p);
            }
        }
        else
        {
            Console.WriteLine("Players: Empty - on " + s.Address);
        }
    }

    public async Task<Server> CreateServerInfoAsync(String address, bool duplicate = false)
    {
        Server s = new Server(address);

        string server = await _httpWrapper.UrtHttpClient.GetStringAsync($"/servers/{address}/");
        var doc = new HtmlDocument();
        doc.LoadHtml(server);

        var boardPath = @"//center/div/div[@id='board']/div[@id='main']/div[@id='mainBox']";
        var boardNode = doc.DocumentNode.SelectSingleNode(boardPath);
        if (boardNode == null)
        {
            Console.WriteLine("BoardNode was missing on: " + address);
            return null;
        }

        s.Address = address;

        var nameNode = boardNode.SelectSingleNode("h2");
        if (nameNode == null)
        {
            Console.WriteLine("NameNode was missing on: " + s.Address);
            return null;
        }

        s.Name = nameNode.InnerText;
        
        var currentMap = boardNode.SelectSingleNode("//table[@class='infos']//tr[td/b[text()='Map:']]/td/text()");
        s.CurrentMap = currentMap.InnerText ?? "";
        
        var flag = boardNode.SelectSingleNode("//table[@class='infos']//tr[td/b[text()='Country:']]/td/span");

        s.Flag = flag.GetAttributeValue("alt","");
        
        var country = boardNode.SelectSingleNode("//table[@class='infos']//tr[td/b[text()='Country:']]/td/text()");
        s.Country = country.InnerText ?? "";

        var gameType = boardNode.SelectSingleNode("//table[@class='infos']//tr[td/b[text()='Game type:']]/td[2]/b/text()");
        s.GameType = gameType.InnerText ?? "";

        return s;


    }

    /// <summary>
    /// Scraps Info from 
    /// </summary>
    /// <returns></returns>
    public async Task<ServerPlayers> GetServerPlayersAsync(string address) 
    {
        // Delay every request
        await Task.Delay(250);
        
        // result list
        ServerPlayers result = new ServerPlayers(address);
        
        string server = await _httpWrapper.UrtHttpClient.GetStringAsync($"/servers/{address}/");
        var doc = new HtmlDocument();
        doc.LoadHtml(server);

        var boardPath = @"//center/div/div[@id='board']/div[@id='main']/div[@id='mainBox']";
        var boardNode = doc.DocumentNode.SelectSingleNode(boardPath);
        if (boardNode == null)
        {
            Console.WriteLine("BoardNode was missing on: " + address);
            return null;
        }

        var nameNode = boardNode.SelectSingleNode("h2");
        if (nameNode == null)
        {
            Console.WriteLine("NameNode was missing on: " + address);
            return null;
        }

        result.Server.Name = nameNode.InnerText;

        var currentMap = boardNode.SelectSingleNode("//table[@class='infos']//tr[td/b[text()='Map:']]/td/text()");
        result.Server.CurrentMap = currentMap.InnerText ?? "";
        
        var flag = boardNode.SelectSingleNode("//table[@class='infos']//tr[td/b[text()='Country:']]/td/span");

        result.Server.Flag = flag.GetAttributeValue("alt","");
        
        var country = boardNode.SelectSingleNode("//table[@class='infos']//tr[td/b[text()='Country:']]/td/text()");
        result.Server.Country = country.InnerText ?? "";

        var gameType = boardNode.SelectSingleNode("//table[@class='infos']//tr[td/b[text()='Game type:']]/td[2]/b/text()");
        result.Server.GameType = gameType.InnerText ?? "";

        var playerBoard = boardNode.SelectSingleNode("div[@class='darkerBox']/table[@class='list']");

        if (playerBoard != null)
        {
            //Console.WriteLine($"Players: ");
            var players = playerBoard.Elements("tr");
            for (int i = 1; i < players.Count(); i++)
            {
                var player = players.ElementAt(i);
                var plname = player.ChildNodes[3];

                var p = new Player() { Name = plname.InnerText, Server = null };
                result.Players.Add(p);
            }
        }
        else
        {
            Console.WriteLine("Players: Empty - on " + address);
        }

        return result;
    }
    // public static async Task CreatePlayerInfoAsync(Player p)
    // {

    //         var user_auth = p.Auth;

    //         var httpResponseMessage = await client.GetStringAsync($"/members/profile/{user_auth}/");
    //         var doc = new HtmlDocument();
    //         doc.LoadHtml(httpResponseMessage);

    //         var lastGamePath = @".//tr[td/b[text()='Last game:']]/td/font/text()|.//tr[td/b[text()='Last game:']]/td/span/text()";
    //         var boardNode = doc.DocumentNode.SelectSingleNode(lastGamePath);
    //         if (boardNode == null)
    //         {
    //             Console.WriteLine("BoardNode was missing on: " + p.Name);
    //             return;
    //         }
    //         Console.WriteLine($"Last seen: {boardNode.InnerText}");

    //         // //Console.WriteLine($"Server name: {s.Name}");
    //         // //Console.WriteLine($"IP: {address}");


    //         //foreach (var x in doc.ParseErrors)
    //         //{
    //         //    Console.WriteLine($"err: {x.SourceText}");
    //         //}

    //         // var boardPath = @"//center/div/div[@id='board']/div[@id='main']/div[@id='mainBox']";
    //         // var boardNode = doc.DocumentNode.SelectSingleNode(boardPath);
    //         // if (boardNode == null)
    //         // {
    //         //     Console.WriteLine("BoardNode was missing on: " + s.Address);
    //         //     return;
    //         // }

    //         // var nameNode = boardNode.SelectSingleNode("h2");
    //         // if (nameNode == null)
    //         // {
    //         //     Console.WriteLine("NameNode was missing on: " + s.Address);
    //         //     return;
    //         // }

    //         // s.Name = nameNode.InnerText;
    //         // //Console.WriteLine($"Server name: {s.Name}");
    //         // //Console.WriteLine($"IP: {address}");

    //         // var playerBoard = boardNode.SelectSingleNode("div[@class='darkerBox']/table[@class='list']");

    //         // if (playerBoard != null)
    //         // {
    //         //     //Console.WriteLine($"Players: ");
    //         //     var players = playerBoard.Elements("tr");
    //         //     for (int i = 1; i < players.Count(); i++)
    //         //     {
    //         //         var player = players.ElementAt(i);
    //         //         var plname = player.ChildNodes[3];

    //         //         var p = new Player() { Name = plname.InnerText, Server = s };
    //         //         tempPlayers.Add(p);
    //         //         if (duplicate)
    //         //             Players.Add(p);
    //         //     }
    //         // }
    //         // else
    //         // {
    //         //     Console.WriteLine("Players: Empty - on " + s.Address);
    //         // }
    //     }
    // }

    public List<Player> FindPlayers(string query)
    {
        var list = new List<Player>();

        query = query.ToLower();
        foreach (var x in Players)
        {
            if (x.Name.ToLower().Contains(query))
            {
                list.Add(x);
            }
        }

        return list;
    }

    public void ScraperRoundEnd()
    {
        Servers = tempServers;
        Players = tempPlayers;

        tempServers = new List<Server>();
        tempPlayers = new List<Player>();
    }
}

