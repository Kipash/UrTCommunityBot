using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using Microsoft.Extensions.Configuration;

namespace UrTCommunityBot.Services
{
    public class HttpClientWraper
    {
        private string _baseAddress;
        private string _loginFormAddress;
        private string _username;
        private string _password;

        public HttpClient UrtHttpClient { get; private set; }

        public HttpClient UrtAuthHttpClient { get; private set; }

        public HttpClientWraper(IConfiguration config)
        {
            _baseAddress = config.GetSection("UrtSite:Uri").Value;
            _loginFormAddress = config.GetSection("UrtSite:LoginUri").Value;
            _username = config.GetSection("UrtSite:Username").Value;
            _password = config.GetSection("UrtSite:Password").Value;   

            this.UrtHttpClient = new HttpClient() { BaseAddress = new Uri(_baseAddress) };
            this.UrtAuthHttpClient = GetAuthLogin();
        }


        // TODO: reconnect after 1 year
        private HttpClient GetAuthLogin()
        {
            
            var handler = new HttpClientHandler() { CookieContainer = new CookieContainer() };
            var client = new HttpClient(handler) { BaseAddress = new Uri(_baseAddress) };

            //usually i make a standard request without authentication, eg: to the home page.
            //by doing this request you store some initial cookie values, that might be used in the subsequent login request and checked by the server
            var homePageResult = client.GetAsync(_loginFormAddress);
            homePageResult.Result.EnsureSuccessStatusCode();

            var content = new FormUrlEncodedContent(new[] {
                    //the name of the form values must be the name of <input /> tags of the login form, in this case the tag is <input type="text" name="username">
                    new KeyValuePair<string, string>("login", _username),
                    new KeyValuePair<string, string>("password", _password),
                    new KeyValuePair<string, string>("remember", "on")
                });

            var loginResult = client.PostAsync(_loginFormAddress, content).Result;

            //loginResult.EnsureSuccessStatusCode();

            return client;
            
        }
    }
}

