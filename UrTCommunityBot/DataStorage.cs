﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace UrTCommunityBot
{
    public class Data
    {
        public string Token { get; set; }
    }

    public static class DataStorage
    {
        static string path;

        public static Data Data { get; private set; }
        
        public static void Load(string path)
        {
            DataStorage.path = path;
            Data = LoadData(path);
        }
        public static void SaveData()
        {
            if (Data == null)
                throw new NullReferenceException("Data cannot be null");

            Save(path, Data);

        }
        static Data LoadData(string path)
        {
            var data = LoadFromFile<Data>(path);

            if (data == null)
                data = new Data();
            if (data.Token == null || string.IsNullOrWhiteSpace(data.Token))
            {
                data.Token = "No token";
            }

            return data;
        }

        public static void Save<T>(string path, T obj) where T : class, new()
        {
            using (var sw = new StreamWriter(path))
            {
                var xs = new XmlSerializer(typeof(T));
                xs.Serialize(sw, obj);
            }
        }
        public static T LoadFromFile<T>(string path) where T : class, new()
        {
            if (File.Exists(path))
            {
                using (var sr = new StreamReader(path))
                {
                    var xs = new XmlSerializer(typeof(T));
                    return (T)xs.Deserialize(sr);
                }
            }
            return new T();
        }
        public static T LoadFromText<T>(string value) where T : class, new()
        {
            if (!string.IsNullOrEmpty(value))
            {
                using (var sr = new MemoryStream(Encoding.UTF8.GetBytes(value)))
                {
                    var xs = new XmlSerializer(typeof(T));
                    return (T)xs.Deserialize(sr);
                }
            }
            return new T();
        }
    }
}