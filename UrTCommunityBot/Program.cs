﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using HtmlAgilityPack;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using UrTCommunityBot.Models;
using UrTCommunityBot.Services;


namespace UrTCommunityBot
{
    class Program
    {
        DiscordSocketClient client;
        CommandService commands;
        IServiceProvider services;
        IConfiguration _config;

        static void Main(string[] args)
        {
            Console.WriteLine("Starting UrT bot");

            // IServiceCollection serviceCollection = new ServiceCollection();
            // ConfigureServices(serviceCollection);

            var p = new Program();
            p.RunBotAsync()
             .GetAwaiter()
             .GetResult();
        }

        // static private void ConfigureServices(IServiceCollection serviceCollection)
        // {
        //     // ILoggerFactory loggerFactory = new Logging.LoggerFactory();
        //     // serviceCollection.AddInstance<ILoggerFactory>(loggerFactory);
        // }

        public async Task RunBotAsync()
        {
            client = new DiscordSocketClient();
            commands = new CommandService();
            
            // Config load
            IConfiguration config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", true, true)
                .Build();

            _config= config;

            services = new ServiceCollection()
                .AddSingleton(client)
                .AddSingleton(commands)
                .AddSingleton(config)
                .AddSingleton<HttpClientWraper>()
                .AddSingleton<UrbanTerrorScraper>()
                .AddDbContext<UrTCommunityBotContext>()
                .BuildServiceProvider();

            Console.WriteLine("Loading appsettings.json...");

            Console.WriteLine("Config loaded");

            var token = config.GetSection("Discord:Token");

            client.Log += Client_Log;

            await RegisterCommandsAsync();
            await client.LoginAsync(TokenType.Bot, token.Value.ToString());
            await client.StartAsync();

            await RunScraperAsync();
            await Task.Delay(-1);
        }

        public async Task RunScraperAsync()
        {
        
            var scrapper = (UrbanTerrorScraper)services.GetService(typeof(UrbanTerrorScraper));
            bool duplicateResults = true;
            while (true)
            {
                CW("Loading map list", ConsoleColor.DarkYellow);
                
                scrapper.LoadAllServers();
                await Task.Delay(3000);

                CW("Scraping servers...", ConsoleColor.DarkGreen);
                foreach (var s in UrbanTerrorScraper.Servers)
                {
                    scrapper.CreateServerInfo(s, duplicateResults);
                    await Task.Delay(3000);
                }

                duplicateResults = false;

                CW("Scraping ended, waiting", ConsoleColor.DarkMagenta);
                scrapper.ScraperRoundEnd();
                await Task.Delay(60000);
            }
            Console.WriteLine("Scraper escaped");
        }

        private Task Client_Log(LogMessage arg)
        {
            Console.WriteLine(arg);
            return Task.CompletedTask;
        }

        public async Task RegisterCommandsAsync()
        {
            client.MessageReceived += HandleCommandAsync;
            await commands.AddModulesAsync(Assembly.GetEntryAssembly(), services);
        }

        public async Task HandleCommandAsync(SocketMessage arg)
        {
            var message = arg as SocketUserMessage;
            var context = new SocketCommandContext(client, message);

            if (message.Author.IsBot) return;

            int argPos = 0;
            
            var commandPrefix = _config.GetSection("Discord:CommandPrefix");

            if (message.HasStringPrefix(commandPrefix.Value, ref argPos))
            {
                var result = await commands.ExecuteAsync(context, argPos, services);
                if (!result.IsSuccess) Console.WriteLine(result.ErrorReason);
            }
        }

        void CW(string msg, ConsoleColor col)
        {
            var currCol = Console.ForegroundColor;
            Console.ForegroundColor = col;
            Console.WriteLine(msg);
            Console.ForegroundColor = currCol;
        }
    }
}
