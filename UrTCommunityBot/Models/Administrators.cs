using System;

namespace UrTCommunityBot.Models
{
    public class Administrator
    {
        public int AdministratorId { get; set; }

        public ulong DiscordId { get; set; }

        public string DiscordName { get; set; }

        public bool IsGod { get; set; }
    }
}