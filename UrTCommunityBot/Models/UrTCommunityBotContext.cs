using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace UrTCommunityBot.Models
{
    public class UrTCommunityBotContext : DbContext
    {
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<Server> Servers { get; set; }
        public virtual DbSet<Clan> Clans { get; set; }
        public virtual DbSet<Administrator> DiscordAdmins { get; set; }

        // public UrTCommunityBotContext(DbContextOptions<UrTCommunityBotContext> options)
        //     :base(options)
        // { }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite("Data Source=Scrapper.db");
    }
}