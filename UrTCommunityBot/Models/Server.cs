namespace UrTCommunityBot.Models
{
    public class Server
    {
        public string ServerId { get; set; }

        public string Address { get; set; }
        
        string name;
        public string Name
        {
            get
            {
                string ret_name = "";
                if (!string.IsNullOrWhiteSpace(name) && name.Length > 0)
                {
                    ret_name = name.Replace("`", @"'");
                }
                return ret_name.Length > 0 ? ret_name : "Unnamed player";
            }
            set
            {
                name = value;
            }
        }

        public string Country { get; set; }
        public string Flag { get; set; }
        public string CurrentMap { get; set; }
        public string GameType { get; set; }
        public Server(string address)
        {
            Address = address;
        }
    }
}