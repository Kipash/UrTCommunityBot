using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace UrTCommunityBot.Models
{
    public class Clan
    {
        public string ClanId { get; set; }

        public string Name { get; set; }

        public string Tag { get; set; }

        [ForeignKey("LeaderClanId")]
        public virtual List<Player> Leaders { get; set; }

        [ForeignKey("MemberClanId")]
        public virtual List<Player> Members { get; set; }

        public virtual List<Server> Servers { get; set; }

        public Clan() 
        {
            Leaders = new List<Player>();
            Members = new List<Player>();
            Servers = new List<Server>();
        }
    }
}