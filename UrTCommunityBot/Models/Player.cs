using System;
using System.Collections.Generic;

namespace UrTCommunityBot.Models
{
    public class Player
    {
        string name;
        public Server Server;
        
        public string PlayerId { get; set; }

        public ulong DiscordId { get; set; }

        public string Auth { get; set; }

        public string Name
        {
            get
            {
                string ret_name = "";
                if (!string.IsNullOrWhiteSpace(name) && name.Length > 0)
                {
                    ret_name = name.Replace("`", @"'");
                    ret_name = name.Replace("*", @"'");
                    ret_name = name.Replace("_", @"-");
                }
                return ret_name.Length > 0 ? ret_name : "Unnamed player";
            }
            set
            {
                name = value;
            }
        }

        public string Gender { get; set; }

        public string Flag { get; set; }

        private string lastseen;
        public string LastSeen
        {
            get 
            {
                string ret_lastseen = "";
                if (!String.IsNullOrEmpty(lastseen))
                {
                    ret_lastseen = lastseen.Replace("&nbsp;", @" ");
                }
                return ret_lastseen; 
            }
            set { lastseen = value; }
        }

        public DateTime LastDateTime { get; set; } = DateTime.MinValue;
    
        public List<Server> FavoriteServers { get; set; } = new List<Server>();

    }
}