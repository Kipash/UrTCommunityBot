using System.Collections.Generic;

namespace UrTCommunityBot.Models
{
    public class ServerPlayers
    {
        public Server Server { get; set; }
        public List<Player> Players { get; set; }

        public ServerPlayers(string address)
        {
            this.Server = new Server(address);
            this.Players = new List<Player>();
        }
    }
}