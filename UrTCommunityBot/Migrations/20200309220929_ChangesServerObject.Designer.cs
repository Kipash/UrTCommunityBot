﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using UrTCommunityBot.Models;

namespace UrTCommunityBot.Migrations
{
    [DbContext(typeof(UrTCommunityBotContext))]
    [Migration("20200309220929_ChangesServerObject")]
    partial class ChangesServerObject
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.2");

            modelBuilder.Entity("UrTCommunityBot.Models.Administrator", b =>
                {
                    b.Property<int>("AdministratorId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<ulong>("DiscordId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("DiscordName")
                        .HasColumnType("TEXT");

                    b.Property<bool>("IsGod")
                        .HasColumnType("INTEGER");

                    b.HasKey("AdministratorId");

                    b.ToTable("DiscordAdmins");
                });

            modelBuilder.Entity("UrTCommunityBot.Models.Clan", b =>
                {
                    b.Property<string>("ClanId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Tag")
                        .HasColumnType("TEXT");

                    b.HasKey("ClanId");

                    b.ToTable("Clans");
                });

            modelBuilder.Entity("UrTCommunityBot.Models.Player", b =>
                {
                    b.Property<string>("PlayerId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Auth")
                        .HasColumnType("TEXT");

                    b.Property<ulong>("DiscordId")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Flag")
                        .HasColumnType("TEXT");

                    b.Property<string>("Gender")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("LastDateTime")
                        .HasColumnType("TEXT");

                    b.Property<string>("LastSeen")
                        .HasColumnType("TEXT");

                    b.Property<string>("LeaderClanId")
                        .HasColumnType("TEXT");

                    b.Property<string>("MemberClanId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.HasKey("PlayerId");

                    b.HasIndex("LeaderClanId");

                    b.HasIndex("MemberClanId");

                    b.ToTable("Players");
                });

            modelBuilder.Entity("UrTCommunityBot.Models.Server", b =>
                {
                    b.Property<string>("ServerId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Address")
                        .HasColumnType("TEXT");

                    b.Property<string>("ClanId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Country")
                        .HasColumnType("TEXT");

                    b.Property<string>("CurrentMap")
                        .HasColumnType("TEXT");

                    b.Property<string>("Flag")
                        .HasColumnType("TEXT");

                    b.Property<string>("GameType")
                        .HasColumnType("TEXT");

                    b.Property<string>("Name")
                        .HasColumnType("TEXT");

                    b.Property<string>("PlayerId")
                        .HasColumnType("TEXT");

                    b.HasKey("ServerId");

                    b.HasIndex("ClanId");

                    b.HasIndex("PlayerId");

                    b.ToTable("Servers");
                });

            modelBuilder.Entity("UrTCommunityBot.Models.Player", b =>
                {
                    b.HasOne("UrTCommunityBot.Models.Clan", null)
                        .WithMany("Leaders")
                        .HasForeignKey("LeaderClanId");

                    b.HasOne("UrTCommunityBot.Models.Clan", null)
                        .WithMany("Members")
                        .HasForeignKey("MemberClanId");
                });

            modelBuilder.Entity("UrTCommunityBot.Models.Server", b =>
                {
                    b.HasOne("UrTCommunityBot.Models.Clan", null)
                        .WithMany("Servers")
                        .HasForeignKey("ClanId");

                    b.HasOne("UrTCommunityBot.Models.Player", null)
                        .WithMany("FavoriteServers")
                        .HasForeignKey("PlayerId");
                });
#pragma warning restore 612, 618
        }
    }
}
