﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace UrTCommunityBot.Migrations
{
    public partial class ChangesServerObject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "GameType",
                table: "Servers",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "GameType",
                table: "Servers");
        }
    }
}
